<?php

require_once('class.db.php');
require_once('class.auth.php');
global $voldb;
$voldb = new DB();
require_once('functions.php');
require_once('class.character.php');

if(isset($_GET['action'])) {
	session_start();
	if($_GET['action'] == 'logout'){
		unset($_SESSION);
		session_destroy();
	}
}

if(isset($_POST['vol_nonce']) && nonce_auth($_POST['vol_nonce'])) {
	$auth = new auth();
	$auth->login($_POST['username'],$_POST['password']);
} else {
	session_start();
	if(isLoggedIn()) {
		echo "LOGGED IN!!!<br>";
		echo "<a href=\"?action=logout\">Logout</a>";
	} else {
		session_start();
		require_once('form1.inc');
	}
}

?>