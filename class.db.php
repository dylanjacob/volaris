<?php

class DB {

		private $dbuser;
		
		private $dbpass;
		
		private $dbname;
		
		private $dbhost;
		
		private $con;
		
		private $last_query = null;
		
		private $last_result = null;
		
		private $num_rows = null;
	
		function __construct() {
			#register_shutdown_function(destroy($this));
						
			require_once('config.php');
			
			$this->dbuser = DB_USER;
			
			$this->dbpass = DB_PASS;
			
			$this->dbname = DB_NAME;
			
			$this->dbhost = DB_HOST;
		}
	
		private function dbConnect() {
			
			try {
				if (isset($this->con)) {
					throw new Exception('Database connection already established');
				} else {
					$this->con = @mysql_connect($this->dbhost, $this->dbuser, $this->dbpass);
					if (!$this->con) {
						throw new Exception('Could not connect to database: '.mysql_error());
					}
					mysql_select_db($this->dbname);
				}
			} catch (Exception $e) {
				echo 'Caught Exception: '. $e->getMessage(). "\n";
			}
		}
		
		public function volQuery() {
			$args = func_get_args();
			$query = $this->prepareQuery($args);
			
			$this->last_query = $query;
			$this->dbConnect();
			try {
				$this->last_result = mysql_query($query,$this->con);
				if (!$this->last_result)
					throw new Exception(mysql_errno().': '.mysql_error());
			} catch (Exception $e) {
				echo 'Caught Exception: '. $e->getMessage(). "\n";
			}
			$this->num_rows = mysql_num_rows($this->last_result);
		}
		
		public function prepareQuery($args) {
			#$args = func_get_args();
			#if (($num = func_num_args()) >1)
				$query = call_user_func_array("sprintf", $args);
			return $query;
		}
		
		public function volInsert($table, $args) {
 			$this->dbConnect();
			$values = array_map('mysql_real_escape_string', array_values($args));
			$keys = array_keys($args);
			$query = 'INSERT INTO `'.$table.'` (`'.implode('`,`', $keys).'`) VALUES (\''.implode('\',\'', $values).'\')';
			
			$this->last_query = $query;
			$this->last_result = mysql_query($query, $this->con);
		}
		
		public function getLastResult() {
			return mysql_fetch_array($this->last_result);
		}
		
		public function getLastQuery() {
			return $this->last_query;
		}
		
		public function getNumRows() {
			return $this->num_rows;
		}
}