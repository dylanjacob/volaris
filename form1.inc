<?php 
if(isLoggedIn()) {
	die("This is oh so wrong, but oh so right...");
}
?>
<html>
<head>
	<title>Volaris Login</title>
</head>
<body>
<h1>Login</h1>
<form name="login" action="index.php" method="POST">
	<div class="tableset">
		<?php nonce(); ?>
		<label for="username">Username:</label>
		<div class="field">
			<input type="text" name="username" />
		</div>
		<label for="password">Password:</label>
		<div class="field">
			<input type="password" name="password" />
		</div>
	</div>
	<div class="tableset">
		<div class="field">
			<input type="submit" value="Login" />
		</div>
	</div>
</form>
</body>
</html>