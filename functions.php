<?php

function nonce_auth($nonce) {
	if(!$nonce)
		return false;
	if($nonce == md5($_SERVER['ORIG_PATH_INFO']))
		return true;
	return false;
}

function nonce() {
	$nonce = md5($_SERVER['PHP_SELF']);
	echo "<input type=\"hidden\" name=\"vol_nonce\" value=\"".$nonce."\" />\n";
}

function isLoggedIn() {
	if (isset($_SESSION['login']) && !empty($_SESSION['login']))
		return true;
	return false;
}