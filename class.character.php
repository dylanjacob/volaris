<?php

class Character {
	protected $charid = null;
	
	function __construct($id=null,$args=array()) {
		if(!is_null($id)) {
			$this->loadCharacter($id);
		} elseif (is_array($args)) {
			$this->createCharacter($args);
		}
	}
	
	public function getID() {
		return $this->charid;
	}
	
	private function loadCharacter($id) {
		$this->charid = $id;
	}
		
	private function createCharacter($args) {
		global $voldb;
		$defaults = array(
			'Name' => '',
			'Class' => 0,
			'Level' => 0,
			'Race' => 0,
			'PlayerID' => 0
		);
		$r = array_merge($defaults, $args);
		
		$voldb->volInsert("Characters",$r);
	}
}
function newCharacter($args) {
	
	$character = new Character(null,$args);
	return $character;
}

function getCharacter($charid) {
	$character = new Character($charid);
	return $character;
}

class Information extends Character {
	
}
?>